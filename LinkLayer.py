
# http://en.wikipedia.org/wiki/Address_Resolution_Protocol
# Internet Protocol (IPv4) over Ethernet ARP packet
#Hardware type (HTYPE)
#Protocol type (PTYPE)
#Hardware address length (HLEN)	Protocol address length (PLEN)
#Operation (OPER)
#Sender hardware address (SHA) (first 16 bits)
#(next 16 bits)
#(last 16 bits)
#Sender protocol address (SPA) (first 16 bits)
#(last 16 bits)
#Target hardware address (THA) (first 16 bits) (ignored for reqest)
#(next 16 bits)
#(last 16 bits)
#Target protocol address (TPA) (first 16 bits)
#(last 16 bits)


__author__ = 'erik'

from functools import partial as partialFunct
import struct

from Interfaces import IPacket, ILayer


class l2Packet(IPacket):
    def __init__(self, l3packet, src_hwaddr=0x00, dst_hwaddr=0x00):
        self.src_hwaddr = src_hwaddr
        self.dst_hwaddr = dst_hwaddr
        self.payload = l3packet
        super(l2Packet, self).__init__(l3packet, src_hwaddr, dst_hwaddr)

    def _data(self):
        return struct.pack('iii', self.src_hwaddr, self.dst_hwaddr, self.chksum) + self.payload.data()
    data = property(_data)

    def __int__(self):
        return int(self.src_hwaddr + self.src_hwaddr + self.payload.chksum)


# IPv4 Header Format
# Offsets	Octet	0	1	2	3
# 0	0	Version(4)   IHL(4)   DSCP(6)	ECN(2)	Total Length(8)
# 4	32	Identification(8)	Flags(3)	Fragment Offset(13)
# 8	64	Time To Live(4)	Protocol(4)	Header Checksum(8)
# 12	96	Source IP Address(32)
# 16	128	Destination IP Address(32)
# 20	160	Options (if IHL > 5)(32)
# Version=4
# IHL = n; where n*32 = sizeof(header)
# ECN = Explicit Congestion Notification (ECN)
# Differentiated Services Code Point (DSCP)
# Protocol = Protocol#; 141-252 unassigned, 253-254 for experiment/testing
# Naive implentation...
class IPPacket(l2Packet):
    def __init__(self, l3packet, src_hwaddr=0x00, dst_hwaddr=0x00):
        self.src = l3packet.src
        self.dst = l3packet.dst
        super(IPPacket, self).__init__(l3packet, src_hwaddr, dst_hwaddr)

    def _data(self):
        return struct.pack('iiiii', self.src_hwaddr, self.src, self.dst_hwaddr,
                                    self.dst, self.chksum) + self.payload.data
    data = property(_data, l2Packet._setdata, l2Packet._deldata)

    def __int__(self):
        return int(self.src_hwaddr + self.src + self.dst_hwaddr + self.dst)

class LinkArpPacket(l2Packet):
    def __init__(self, link_arp_packet, src_mac=0x00, dst_mac=0xFF):
        self.HTYPE = link_arp_packet.hw_type
        self.PTYPE = link_arp_packet.proto_type
        self.HLEN = link_arp_packet.waddr_len
        self.PLEN = link_arp_packet.proto_len
        self.OPER = link_arp_packet.operation
        self.src_ip = link_arp_packet.src_ip
        self.dst_ip = link_arp_packet.dst_ip
        super(LinkArpPacket, self).__init__(None, src_mac, dst_mac)

    def _data(self):
        return struct.pack("iihhiii", [self.HTYPE,
                                       self.PTYPE,
                                       self.HLEN,
                                       self.PLEN,
                                       self.OPER,
                                       self.src_hwaddr,
                                       self.src_ip,
                                       self.dst_ip,
                                       self.dst_hwaddr])

    data = property(_data, l2Packet._setdata, l2Packet._deldata)

    def __int__(self):
        return sum([self.HTYPE, self.PTYPE,
                    self.HLEN, self.PLEN,
                    self.OPER,
                    self.src_hwaddr, self.src_ip,
                    self.dst_ip, self.dst_hwaddr])

## generic arp packet Def
# HTYPE := 1 ethernet
# PTYPE := 0x0800 ipv4
# HLEN := 6 ethernet
# PLEN := 4 IPV4
# OPER := 1 request; OPER :=2  reply
class LinkLayer(ILayer):
    def __init__(self, layer1, my_hwaddr=0x00):
        # TODO: assert mac is valid, layer1 is valid
        self.hwaddr = my_hwaddr
        self.layer1 = layer1
        self.l2Packet = partialFunct(l2Packet, src_hwaddr=my_hwaddr)
        super(LinkLayer, self).__init__(layer1, my_hwaddr)

    def send(self, l3Packet):
        self.layer1.send(self.l2Packet(l3Packet, dst_hwaddr=self.hwaddr))

    def recv(self, l1packet):
        #TODO: send l1packet.data up
        pass


class ArpLinkLayer(LinkLayer):
    def __init__(self, layer1, layer3, my_hwaddr=0x00):
        self._arptable = {}
        self._defer = []
        self._l2Packet = partialFunct(l2Packet, src_hwaddr=my_hwaddr)
        self._LinkArpPacket = partialFunct(LinkArpPacket, src_hwaddr=my_hwaddr)
        super(ArpLinkLayer, self).__init__(layer1, my_hwaddr)


    def send(self, ip_packet, dst=0x00):
        if getattr(ip_packet, "OPER") == 1:
            arp_packet = self._LinkArpPacket(ip_packet, dst_hwaddr=dst)
        if not dst:
            return False
        else:
            super(ArpLinkLayer, self).send(self._l2Packet(ip_packet, dst_hwaddr=dst))
            return True

    def recv(self, l1packet):
        l2packet = l1packet.payload
        # arp response
        if isinstance(l2packet, LinkArpPacket) and getattr(l2packet, "OPER") == 2:
            # TODO: SENDUP
            return l2packet
        if not l2packet.dst_hwaddr == self.hwaddr:
            return False
        else:
            return l2packet
