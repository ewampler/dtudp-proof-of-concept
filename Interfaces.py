__author__ = 'erik'


class IPacket(object):
    def __init__(self, *args, **kwargs):
        super(IPacket, self).__init__()

    def _data(self):
        return ""
    def _setdata(self, data):
        self._lazy_checksum = 0
    def _deldata(self):
        self._lazy_checksum = 0
    data = property(_data, _setdata, _deldata)

    def _chksum(self):
        if not getattr(self, "_lazy_checksum", 0):
            self._lazy_checksum = int(self)
        return self._lazy_checksum
    chksum = property(_chksum)

    def __int__(self):
        return 0

class ILayer(object):
    def __init__(self, *args, **kwargs):
        if super(ILayer, self) is not object:
            super(ILayer, self).__init__(*args, **kwargs)
        else:
            super(ILayer, self).__init__()