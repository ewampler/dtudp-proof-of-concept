__author__ = 'erik'

#http://en.wikipedia.org/wiki/User_Datagram_Protocol#Packet_structure
import struct
from functools import partial as partialFunction
from collections import defaultdict
from Interfaces import IPacket, ILayer

class TransportError(Exception):
    pass

class connection(object):
    def __init__(self, type):
        self.type = None

class l4Packet(IPacket):
    def __init__(self, l5packet, src_port=0xff, dst_port=0xff):
        self.src_port = src_port
        self.dst_port = dst_port
        self.connection = None
        self.payload = l5packet
        super(l4Packet, self).__init__(l5packet)

    def _data(self):
        return struct.pack('ii', self.src_port, self.dst_port) + self.payload.data

    data = property(_data, IPacket._setdata, IPacket._deldata)

    def __int__(self):
        return int(self.src_port + self.dst_port) + self.payload.chksum


class UDPPacket(IPacket):
    int_length = len(struct.pack('i', 0))

    def __init__(self, l5packet, src_port=0xff, dst_port=0xff):
        self.payload = l5packet
        self.src_port = src_port
        self.dst_port = dst_port
        super(UDPPacket, self).__init__(l5packet, src_port, dst_port)

    def _data(self):
        pdata = self.payload.data
        return struct.pack('iii', self.src_port, self.dst_port, len(pdata)) + pdata

    def _setdata(self, data):
        header, data = data[:UDPPacket.int_length], data[UDPPacket.int_length:]
        self.src_port, self.destport, self.length, chksum = struct.unpack("iiii", header)
        self.payload[:] = data
        IPacket._setdata(self, data)

    data = property(_data, _setdata, IPacket._deldata)

    def __int__(self):
        return int(self.src_port + self.dst_port) + self.payload.chksum


class TransportLayer(ILayer):
    def __init__(self, layer3, hostname=0xFF):
        self.hostname = hostname
        self.connections = defaultdict(tuple)
        self.layer3 = layer3
        self._l4Packet = l4Packet
        super(TransportLayer, self).__init__(layer3)

    def NewConnection(self, dst_ip, dst_port):
        socketID = next(x for x in xrange(10000) if x not in self.connections)
        self.connections[socketID] = (dst_ip, dst_port)
        return socketID


    def send(self, data, socketID):
        if not socketID in self.connections:
            raise TransportError("No ip associated with SocketID %d" % socketID)

        dest_ip, dest_port = self.connections[socketID]
        self.layer3.send(self._l4Packet(data, dest_port), dest_ip)

    def recv(self, l3packet):
        payload = l3packet.payload

