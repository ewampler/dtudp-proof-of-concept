__author__ = 'erik'


eventQueue = []


class Client(object):
    def __init__(self):
        assert getattr(self, "mac", 0) > 0x00 and getattr(self, "mac") < 0xFF, \
            "INVALID CLIENT: No MAC exists for client"


    def send(self, l1packet):
        Router().sendPacket(l1packet)

    def recv(self, l1packet):
        raise NotImplementedError("recv not implemented for %s" % self)


class Router(object):
    _state = {}
    def __new__(cls, *p, **k):
        self = object.__new__(cls, *p, **k)
        self.__dict__ = cls._state
        return self

    def __init__(self):
        self._ports = {}


    def Connect(self, client):
        # this is a faux static typing polymorphism check
        assert isinstance(client, Client), "ROUTER CONNECTION: Invalid client type for connection %s" % client
        self._ports[client.hwaddr] = client

    def sendPacket(self, l1packet):
        self._ports[l1packet] = l1packet.dst

    def recv(self, data):
        pass
        # read data header
