__author__ = 'erik'

from functools import partial as partialFunct
from Interfaces import IPacket, ILayer
from collections import defaultdict
import struct
#### http://en.wikipedia.org/wiki/IPv4_header#Header
def randOctect():
    from random import randint
    return randint(1, 255)

# packet(srcmac, dstmac, checksum, data, payload)
# node.send(node.layer1.send


class l3Packet(IPacket):
    def __init__(self, l4packet, src, dst):
        self.payload = l4packet
        self.src = src
        self.dst = dst
        super(l3Packet, self).__init__(src, dst)

    def _data(self):
        return struct.pack('iii', self.src, self.dst) + self.payload.data()
    data = property(_data, IPacket._setdata, IPacket._deldata)


class IPPacket(l3Packet):
    def __init__(self, l4packet, src_ip=0x00, dst_ip=0x00):
        #NOTE: assert IP Standard here or change address to something.
        # in naive model, doesn't matter
        super(IPPacket, self).__init__(l4packet, src_ip, dst_ip)


class NetworkArpPacket(IPPacket):
    def __init__(self, src_ip=0x00, dst_ip=0x00,
                 hw_type=1, proto_type=0x08800,
                 hwaddr_len=6, proto_len=4, operation=1):
        self.HTYPE = hw_type
        self.PTYPE = proto_type
        self.HLEN = hwaddr_len
        self.PLEN = proto_len
        self.OPER = operation
        self.src_ip = src_ip
        self.dst_ip = dst_ip
        super(NetworkArpPacket, self).__init__(None, src_ip, dst_ip)

    def _data(self):
        return struct.pack("iihhiii", [self.HTYPE,
                                       self.PTYPE,
                                       0x00,
                                       self.PLEN,
                                       self.OPER,
                                       0x00,
                                       self.src_ip,
                                       self.dst_ip,
                                       0x00])
    data = property(_data)

    def __int__(self):
        return sum([self.HTYPE, self.PTYPE,
                    self.HLEN, self.PLEN,
                    self.OPER,
                    self.src_hwaddr, self.src_ip,
                    self.dst_ip, self.dst_hwaddr])


class NetworkLayer(ILayer):
    def __init__(self, layer2, ip_addr=0x00):
        self.ip_addr = ip_addr
        # assert layer2 is valid, ipAddress is valid here
        self.layer2 = layer2
        self._l3Packet = partialFunct(l3Packet, src=ip_addr)
        super(NetworkLayer, self).__init__(self, layer2, ip_addr)

    def send(self, l4packet, dst):
        self.layer2.send(self._l3Packet(l4packet, dst=dst), dst)

    def recv(self, l2packet):
        l3packet = l2packet.payload
        if l3packet.dst == self.ip_addr:
            return l3packet
        return False


class IPNetworkLayer(NetworkLayer):
    def __init__(self, layer2, ip_addr=0x00):
        self.ip_addr = ip_addr
        # assert layer2 is valid, ipAddress is valid here
        self.layer2 = layer2
        self._IPPacket = partialFunct(IPPacket, ipAdd=ip_addr)
        super(IPNetworkLayer, self).__init__(layer2, ip_addr)

    # naive IP: no arp needed for MAC == IP will work for us
    def send(self, l4packet, dst):
        l3packet = l4packet.payload
        return self.layer2.send(self._IPPacket(l4packet, dst)) is True

    def recv(self, l2packet):
        l3packet = l2packet.payload
        if l3packet.dst == 0xFF or l3packet.dst == self.ip_addr:  # for me
            return l3packet
        else:
            self.layer2.send(l2packet)  # Not My problem
            return True


class ArpIPNetworkLayer(IPNetworkLayer):
    def __init__(self, layer2, ip_addr=0x00):
        self._arp_table = defaultdict(lambda: 0x00)
        self._deferred = []
        self._reqArpPacket = partialFunct(NetworkArpPacket, src_ip=self.ip_addr, operation=1)
        self._respArpPacket = partialFunct(NetworkArpPacket, src_ip=self.ip_addr, operation=2)
        super(ArpIPNetworkLayer, self).__init__(layer2, ip_addr)

    def send(self, l4packet, dst):
        # Check if it's in our arp cache (a defaultdict will create an entry of 0x00 for us if it is not
        if not self._arp_table[dst]:
            # create an Arprequest and send it down
            self.layer2.send(self._reqArpPacket(dest_ip=dst), 0xFF)
            # defer the packet send attempt until we get the arp address
            self._deferred.append(partialFunct(self.send, l4packet, dst))
            return None
        # go ahead and send it if it's in the arp
        return self.layer2.send(self._l3Packet(l4packet.payload, self._arp_table[dst])) is True

    def recv(self, l2packet):
        l3packet = l2packet.payload
        if l3packet.dest_ip == self.ip_addr and hasattr(l3packet, "OPER"):
            self._arp_table[l3packet.src_ip] = l3packet.src_hwaddrs
            # received back arp response
            if l3packet.OPER == 2:
                # TODO: better defer method than this race condition
                self._deferred[:] = [d for d in self._deferred[:] if not d()]
                return True  # taken care of, stop
            # send out arp response
            elif l3packet.OPER == 1 and l3packet.dst_ip == self.ip_addr:
                self.layer2.send(self._respArpPacket(dest_ip=l3packet.src), 0x00)
                return True  # taken care of, stop
        return super(ArpIPNetworkLayer, self).recv(l2packet)